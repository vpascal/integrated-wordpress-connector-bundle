<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\WordpressConnectorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @author Ger Jan van den Bosch <gerjan@e-active.nl>
 */
class ConfigurationType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'url',
            'text',
            array(
                'label' => 'Wordpress website URL',
                'attr' => array(
                    'placeholder' => 'http://site.com/xmlrpc.php',
                ),
            )
        );
        $builder->add('username', 'text', array('label' => 'Username'));
        $builder->add('password', 'password', array('label' => 'Password'));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'integrated_wordpress_connector_configuration';
    }
}
