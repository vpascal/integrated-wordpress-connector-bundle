<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\WordpressConnectorBundle\Connector;

use Integrated\Common\Channel\Connector\Adapter\ManifestInterface;
use Integrated\Common\Channel\Connector\AdapterInterface;
use Integrated\Common\Channel\Connector\Config\OptionsInterface;
use Integrated\Common\Channel\Connector\ConfigurableInterface;
use Integrated\Common\Channel\Connector\ConfigurationInterface;
use Integrated\Common\Channel\Exporter\ExportableInterface;
use Integrated\Common\Channel\Exporter\ExporterInterface;

/**
 * @author Vasil Pascal <developer.optimum@gmail.com>
 */
class WordpressConnectorAdapter implements AdapterInterface, ConfigurableInterface, ExportableInterface
{
    /**
     * @var ManifestInterface
     */
    private $manifest;

    /**
     * @var ConfigurationInterface
     */
    private $configuration;

    /**
     * @var ExporterInterface
     */
    private $exporter;

    /**
     * WordPressConnectorAdapter constructor.
     *
     * @param ManifestInterface      $manifest
     * @param ConfigurationInterface $configuration
     * @param ExporterInterface      $exporter
     */
    public function __construct(
        ManifestInterface $manifest,
        ConfigurationInterface $configuration,
        ExporterInterface $exporter
    ) {
        $this->manifest = $manifest;
        $this->configuration = $configuration;
        $this->exporter = $exporter;
    }

    /**
     * {@inheritdoc}
     */
    public function getManifest()
    {
        return $this->manifest;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * {@inheritdoc}
     */
    public function getExporter(OptionsInterface $options)
    {
        return $this->exporter;
    }
}
