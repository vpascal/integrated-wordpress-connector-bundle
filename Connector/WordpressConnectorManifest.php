<?php

/*
 * This file is part of the Integrated package.
 *
 * (c) e-Active B.V. <integrated@e-active.nl>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Integrated\Bundle\WordpressConnectorBundle\Connector;

use Integrated\Common\Channel\Connector\Adapter\ManifestInterface;

/**
 * Class WordpressConnectorManifest
 * @author Vasil Pascal <developer.optimum@gmail.com>
 */
class WordpressConnectorManifest implements ManifestInterface
{
    const NAME = 'wordpress';

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return self::NAME;
    }

    /**
     * {@inheritdoc}
     */
    public function getLabel()
    {
        return 'WordPress';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'Configure Wordpress exporter';
    }

    /**
     * {@inheritdoc}
     */
    public function getVersion()
    {
        return '1.0';
    }
}
